package com.example.webapp.controller;

import com.example.webapp.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping("/hello")
    public ResponseEntity<Message> hello() {
        return new ResponseEntity<>(new Message("Hello, World!"), HttpStatus.OK);
    }
}
